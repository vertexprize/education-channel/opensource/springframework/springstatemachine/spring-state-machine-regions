/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.statemachine.config;

import java.util.Arrays;
import java.util.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

/**
 *
 * @author vaganovdv
 */
@Configuration
@EnableStateMachine
public class RegionConfiguration extends StateMachineConfigurerAdapter<String, String> {

     @Autowired
    private StateMachineLogListener stateMachineLogListener;
    
    @Override
    public void configure(StateMachineConfigurationConfigurer<String, String> config)
            throws Exception {
        config
                .withConfiguration()
                .autoStartup(false)
                .machineId("state-machine")
                .listener(stateMachineLogListener);
    }
    
    
    @Override
    public void configure(StateMachineStateConfigurer<String, String> states) throws Exception {

        states.withStates()
                .region("R0")
                .initial("S1")                
                .fork("S2")
                .state("S3")                                                
                .join("S4")
                .state("S5")
                .end("S6")                
                .and()                
                .withStates()
                    .region("R2")
                    .parent("S3")
                    .initial("S20")                         
                    .states(new HashSet<String>(Arrays.asList("S21", "S22", "S23")))
                    .end("S2F")
                    .and()
                .withStates()
                    .region("R3")                    
                    .parent("S3")
                    .initial("S30")
                    .states(new HashSet<String>(Arrays.asList("S31", "S32", "S33")))
                    .end("S3F");                                
                    
                    
    }
    
    @Override
    public void configure(StateMachineTransitionConfigurer<String, String> transitions) throws Exception {
        transitions                
                .withExternal()
                    .source("S1")                
                    .target("S2")
                    .event("E1")
                .and()
                              
                .withFork()
                    .source("S2")
                    .target("S21")
                    .target("S31")
                .and()
                
                 // Region 2
                .withLocal()
                    .source("S21")
                    .target("S22")
                    .event("E2")
                .and()
                .withLocal()
                    .source("S22")
                    .target("S23")
                    .event("E3")
                .and()                               
                .withLocal()
                    .source("S23")
                    .target("S2F")
                    .event("E2-STOP")
                .and()
                // ---------------------------
                
                // ------ Region 3 ---------               
                .withLocal()
                    .source("S31")
                    .target("S32")
                    .event("E4")
                .and()
                .withLocal()
                    .source("S32")
                    .target("S33")
                    .event("E5")
                .and()
                .withLocal()
                    .source("S33")
                    .target("S3F")
                    .event("E3-STOP")
                .and()
                
                // ------- Join ----------------
                .withJoin()
                    .source("S2F")
                    .source("S3F")
                    .target("S4")
                .and()
                .withExternal()
                    .source("S4")
                    .target("S5")
                .and()
                .withExternal()
                    .source("S5")
                    .target("S6")
                    .event("STOP")
                
                
                ;
                    
                
        
    }


}
