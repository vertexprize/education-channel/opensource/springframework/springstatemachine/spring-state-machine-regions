package org.vertexprize.education.spring.statemachine;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineEventResult;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SpringBootApplication
@Slf4j
public class SpringStateMachineApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringStateMachineApplication.class, args);
    }

    @Autowired
    private StateMachine<String, String> stateMachine;

    @PostConstruct
    public void start() {

        log.info("Начальное состояние машины: " + stateMachine.getInitialState().getId());
        
        Message<String> m1 = MessageBuilder.withPayload("E1").build();
        Message<String> m2 = MessageBuilder.withPayload("E2").build();
        Message<String> m3 = MessageBuilder.withPayload("E3").build();
        Message<String> m4 = MessageBuilder.withPayload("E4").build();
        Message<String> m5 = MessageBuilder.withPayload("E5").build();
        Message<String> r2_STOP = MessageBuilder.withPayload("E2-STOP").build();
        Message<String> r3_STOP = MessageBuilder.withPayload("E3-STOP").build();
        Message<String> STOP = MessageBuilder.withPayload("STOP").build();

        log.info("Старт машины состостояний: " + stateMachine.getId());
        stateMachine.getStateMachineAccessor()
                .doWithAllRegions( 
                        s->
                        {
                            s.setInitialEnabled(true);    
                           // s.setForwardedInitialEvent(m1);                            
                        });
        
        Mono<Void> startReactively = stateMachine.startReactively();
        startReactively.block();

        
        log.info("Отправка события группы событий ... ");       
        
        Flux<StateMachineEventResult<String, String>> results
                //= stateMachine.sendEvents(Flux.just(m1));
                //= stateMachine.sendEvents(Flux.just(m1, m2, m4,  m3));
                = stateMachine.sendEvents(Flux.just(m1, m2, m4,  m3, m5,  r3_STOP, r2_STOP, STOP));

        results.subscribe();



    }

    @PreDestroy
    public void stop() {
        log.info("Остановка  машины состостояний ...");
        Mono<Void> startReactively = stateMachine.stopReactively();
        startReactively.subscribe();

    }

}
